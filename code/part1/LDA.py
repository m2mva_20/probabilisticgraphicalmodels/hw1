#!/bin/python3

import os

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import pandas as pd

from utils import loadData, plotData


class Solver:

    def __init__(self):
        self.name  = "LDA"
        self.pi    = 0
        self.mu    = []
        self.sigma = []

    def hyperplane(self, x):
        pi    = self.pi
        mu    = self.mu.copy()
        sigma = self.sigma[0]
        sigma_inv = np.linalg.inv(sigma)
        A = ((mu[0]-mu[1]).T@sigma_inv).reshape(2)
        return (1/A[1]*(-A[0]*x +np.log((pi/(1-pi))) + 0.5*(mu[0].T@sigma_inv@mu[0] - mu[1].T@sigma_inv@mu[1])))[0,0]

    def fit(self, data):
        '''Perform a Linear discrimination analysis on a 2D dataframe with 2 clusters'''
        # Estimation of the label's marginal probability
        pi = sum(data['Y']/len(data))
        # Estimation of cluster's mean
        mu = [np.zeros((2,1)),np.zeros((2,1))]
        n  = [0,0]
        for i in range(len(data)):
            mu[data['Y'][i]] += np.array([[data['X0'][i]], [data['X1'][i]]])
            n[data['Y'][i]]  += 1
        mu[0] /= n[0]
        mu[1] /= n[1]
        # Estimation of cluster's variance
        sigma = [np.zeros((2,2)),np.zeros((2,2))]
        for i in range(len(data)):
            x   = np.array([[data['X0'][i]], [data['X1'][i]]])
            x_c = (x-mu[data['Y'][i]])
            sigma[data['Y'][i]] += x_c@x_c.T/n[data['Y'][i]]
        self.data  = data
        self.pi    = pi
        self.mu    = mu
        self.sigma = sigma
        return pi, mu, sigma

    def accuracy(self, data):
        pi    = self.pi
        mu    = self.mu
        sigma = self.sigma
        false = [len(data)-sum(data['Y']), sum(data['Y'])]
        for i in range(len(data)):
            if data['Y'][i] and data['X1'][i] - self.hyperplane(data['X0'][i]) > 0:
                false[1] -= 1
            elif not data['Y'][i] and data['X1'][i] - self.hyperplane(data['X0'][i]) < 0:
                false[0] -=1
        false[1] /= sum(data['Y'])
        false[0] /= (len(data) - sum(data['Y']))
        return false


def main():
    trainingDatasets = ['trainA', 'trainB', 'trainC']
    testDatasets     = ['testA', 'testB', 'testC']
    DATAPATH = '../../data/'
    for i in range(len(trainingDatasets)):
        print('#'*30 +'\t'+ trainingDatasets[i]+'\t'+'#'*30)
        trainingData = loadData(DATAPATH+trainingDatasets[i])
        testData     = loadData(DATAPATH+testDatasets[i])
        LDA = Solver()
        pi, mu, sigma = LDA.fit(trainingData)
        acc = LDA.accuracy(trainingData)
        print('Accuracy in the training set:\nCluster1: {:1.4f}\tCluster2: {:1.4f}'.format(acc[0], acc[1]))
        plotData(trainingData, mu=mu, sigma=sigma, name='{}_{}'.format(LDA.name, trainingDatasets[i]), sep=LDA.hyperplane)
        acc = LDA.accuracy(testData)
        print('Accuracy in the test set:\nCluster1: {:1.4f}\tCluster2: {:1.4f}'.format(acc[0], acc[1]))
        plotData(testData, mu=mu, sigma=sigma, name='{}_{}'.format(LDA.name, testDatasets[i]), sep=LDA.hyperplane)

if __name__=="__main__":
    main()
