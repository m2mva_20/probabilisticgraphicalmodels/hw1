#!/bin/python3

import math
import os
import warnings

warnings.simplefilter("ignore")


import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import pandas as pd

from utils import loadData, plotData


class LogisticReg:

    def __init__(self, data, **kwargs):

        self.name = 'logistic'

        self.data  = data
        self.n     = len(data)
        self.param = np.array([[0.0], [0], [0.0]]) if 'param' not in kwargs.keys() else kwargs.get('param')

        self.g   = lambda t : 1/(1+math.exp(-t))
        self.dg  = lambda t : self.g(t)*(1-self.g(t))

    def f(self, **kwargs):
        param = self.param if 'param' not in kwargs.keys() else kwargs.get('param')
        w  = param[0:2,:]
        b  = param[2,0]
        r  = 0
        for i in range(self.n):
            x  = np.array([[self.data['X0'][i]], [self.data['X1'][i]]])
            g  = self.g((w.T@x)[0,0]+b)
            y  = self.data['Y'][i]
            r += y*np.log(g) + (1-y)*np.log(1-g)
        return r

    def df(self, **kwargs):
        param = self.param if 'param' not in kwargs.keys() else kwargs.get('param')
        w  = param[0:2,:]
        b  = param[2,0]
        r = np.zeros((w.shape[0]+1, 1))
        for i in range(self.n):
            x  = np.array([[self.data['X0'][i]], [self.data['X1'][i]]])
            g  = self.g((w.T@x)[0,0]+b)
            y  = self.data['Y'][i]
            tmp= (y*(1-g) - (1-y)*g)
            r[0:w.shape[0],:] += tmp*x
            r[w.shape[0], 0]  += tmp
        return r

    def ddf(self, **kwargs):
        param = self.param if 'param' not in kwargs.keys() else kwargs.get('param')
        w  = param[0:2,:]
        b  = param[2,0]
        r  = np.zeros((w.shape[0]+1, w.shape[0]+1))
        for i in range(self.n):
            x  = np.array([[self.data['X0'][i]], [self.data['X1'][i]]])
            dg = self.dg((w.T@x)[0,0]+b)
            y  = self.data['Y'][i]
            r[0:w.shape[0],0:w.shape[0]]  += -dg*x@x.T
            r[0:w.shape[0], w.shape[0]:]  += -dg*x
            r[w.shape[0]:, 0:w.shape[0]]  += -dg*x.T
            r[w.shape[0], w.shape[0]]     += -dg
        return r

    def constraintCheck(self,x):
        w = x[0:2,:]
        b = x[2,0]
        for i in range(self.n):
            x  = np.array([[self.data['X0'][i]], [self.data['X1'][i]]])
            if self.g((w.T@x)[0,0] + b)==1:
                return True
        return False


    def fit(self, eps=1e-7, alpha = 0.25, beta = 0.65):
        '''Compute the centering step thanks to newton algorithm and a backtracking line search'''
        f   = lambda l : -self.f(param=l)
        df  = lambda l : -self.df(param=l)
        ddf = lambda l : -self.ddf(param=l)
        dec = 1
        x   = np.copy(self.param)
        v   = [np.copy(x)]
        D = []
        while dec/2 > eps:
            cdf  = df(x)
            cddf = ddf(x)
            dx   = -np.linalg.inv(cddf).dot(cdf)
            dec  = cdf.T.dot(-dx)
            t    = 1
            while t>1e-12 and ( f(x+t*dx) >= f(x)+alpha*t*cdf.T.dot(dx)):
                t = beta*t
            x  += t*dx
            dec = df(x).T.dot(np.linalg.inv(ddf(x)).dot(df(x)))
            v.append(np.copy(x))
            D.append(dec)
        self.param = np.copy(-x)
        return v, D

    def hyperplane(self, x):
        w  = self.param[0:2,:]
        b  = self.param[2,0]
        return -1/w[1,0]*(w[0,0]*x+b)

    def accuracy(self, data):
        false = [len(data)-sum(data['Y']), sum(data['Y'])]
        for i in range(len(data)):
            if data['Y'][i] and data['X1'][i] - self.hyperplane(data['X0'][i]) > 0:
                false[1] -= 1
            elif not data['Y'][i] and data['X1'][i] - self.hyperplane(data['X0'][i]) < 0:
                false[0] -=1
        false[1] /= sum(data['Y'])
        false[0] /= (len(data) - sum(data['Y']))
        return false


def main():
    trainingDatasets = ['trainA', 'trainB', 'trainC']
    testDatasets     = ['testA', 'testB', 'testC']
    DATAPATH = '../../data/'
    for i in range(len(trainingDatasets)):
        print('#'*30 +'\t'+ trainingDatasets[i]+'\t'+'#'*30)
        # Loading datasets
        trainingData = loadData(DATAPATH+trainingDatasets[i])
        testData     = loadData(DATAPATH+testDatasets[i])
        # Learning the model
        LR = LogisticReg(trainingData)
        v, D = LR.fit()
        # Display the results and compute accuracy
        acc = LR.accuracy(trainingData)
        print('Accuracy in the training set:\nCluster1: {:1.4f}\tCluster2: {:1.4f}'.format(acc[0], acc[1]))
        plotData(trainingData, sep=LR.hyperplane, name='{}_{}'.format(LR.name, trainingDatasets[i]))
        acc = LR.accuracy(testData)
        print('Accuracy in the test set:\nCluster1: {:1.4f}\tCluster2: {:1.4f}'.format(acc[0], acc[1]))
        plotData(testData, sep=LR.hyperplane, name='{}_{}'.format(LR.name, testDatasets[i]))


if __name__=="__main__":
    main()
