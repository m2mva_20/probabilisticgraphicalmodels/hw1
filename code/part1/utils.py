#!/bin/python3

import os

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import pandas as pd


def hyperplane(x, sigma, mu, pi):
    sigma_inv = np.linalg.inv(sigma)
    A = ((mu[0]-mu[1]).T@sigma_inv).reshape(2)
    return 1/A[1]*(-A[0]*x +np.log((pi/(1-pi))) + 0.5*(mu[0].T@sigma_inv@mu[0] - mu[1].T@sigma_inv@mu[1]))

def gauss(x, sigma, mu):
    return np.exp(-0.5*((x-mu).T@np.linalg.inv(sigma)@(x-mu)))

def loadData(path):
    if not os.path.isfile(path):
        print('File {} not found in the system'.format(path))
        return
    data = pd.read_csv(path, sep=' ' ,names = ['X0', 'X1', 'Y'])
    return data

def plotData(data, **kwargs):
    plt.figure()
    plt.scatter(data['X0'], data['X1'],c=data['Y'],alpha=.6, cmap='seismic')
    plt.grid()
    plt.xlabel(r'$X_0$')
    plt.ylabel(r'$X_1$')
    name = '' if 'name' not in kwargs.keys() else kwargs.get('name')
    if 'mu' in kwargs.keys() and 'sigma' in kwargs.keys():
        pi    = kwargs.get('pi')
        sigma = kwargs.get('sigma')
        mu    = kwargs.get('mu')
        x = np.linspace(min(data['X0'])-1, max(data['X0'])+1, 100)
        y = np.linspace(min(data['X1'])-1, max(data['X1'])+1, 100)
        X,Y = np.meshgrid(x,y)
        for j in range(2):
            z1 = np.zeros((100, 100))
            for i,u in enumerate(x):
                for k,v in enumerate(y):
                    z1[k,i] = gauss(np.array([[u],[v]]),sigma[j], mu[j])
            levels = [.05,0.2]
            CS=plt.contour(X,Y,z1,len(levels),linewidths=0.5, colors = ['black'], levels=levels)
            plt.clabel(CS, inline=True, fontsize=10)
            plt.plot(mu[j][0,0], mu[j][1,0], '+', markersize=10)
    if 'sep' in kwargs.keys():
        hyperplane = kwargs.get('sep')
        x = np.linspace(min(data['X0'])-1, max(data['X0'])+1, 10)
        plt.plot(x, [hyperplane(i) for i in x], '--',label='Separating hyperplane')
        plt.xlim(min(data['X0'])-1, max(data['X0'])+1)
        plt.ylim(min(data['X1'])-1, max(data['X1'])+1)
        plt.legend()
    plt.savefig('../../fig/{}.pdf'.format(name))
    plt.show()
