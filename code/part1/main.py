#!/bin/python3


import os

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import pandas as pd

import LDA as part1
import LinReg as part3
import LogisticReg as part2
from utils import loadData, plotData


def main():
    trainingDatasets = ['trainA', 'trainB', 'trainC']
    testDatasets     = ['testA', 'testB', 'testC']
    DATAPATH = '../../data/'
    for i in range(len(trainingDatasets)):
        print('#'*30 +'\t'+ trainingDatasets[i]+'\t'+'#'*30)
        # Loading the datasets
        trainingData = loadData(DATAPATH+trainingDatasets[i])
        testData     = loadData(DATAPATH+testDatasets[i])
        # Learning the model
        LDA = part1.Solver()
        LR  = part2.LogisticReg(trainingData)
        LIR = part3.LinReg(trainingData)
        pi, mu, sigma = LDA.fit(trainingData)
        v, D = LR.fit()
        LIR.fit()
        #Display the results
        sol = [LDA, LR, LIR]
        for s in sol:
            print(s.name)
            acc = s.accuracy(trainingData)
            print('Accuracy in the training set:\nCluster1: {:1.4f}\tCluster2: {:1.4f}'.format(acc[0], acc[1]))
            plotData(trainingData, name=trainingDatasets[i], sep=s.hyperplane)
            acc = s.accuracy(testData)
            print('Accuracy in the test set:\nCluster1: {:1.4f}\tCluster2: {:1.4f}'.format(acc[0], acc[1]))
            plotData(testData, name=testDatasets[i], sep=s.hyperplane)

if __name__=="__main__":
    main()
