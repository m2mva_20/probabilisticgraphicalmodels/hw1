#!/bin/python3

import math
import os
import warnings

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import pandas as pd

from utils import loadData, plotData


class LinReg:
    def __init__(self, data):
        self.name = 'LinReg'
        self.data = data
        self.w    = np.zeros((2,1))
        self.b    = 0

    def fit(self):
        X = np.zeros((len(self.data), 2))
        b = np.zeros((len(self.data), 1))
        for i in range(len(self.data)):
            X[i, 0] = self.data['X0'][i]
            X[i, 1] = self.data['X1'][i]
        self.w = np.linalg.inv(X.T@X)@X.T@self.data['Y']
        for i in range(len(self.data)):
            b[i, 0] = self.data['Y'][i] - X[i,:].T.dot(self.w)
        self.b = np.mean(b)
        return self.w

    def hyperplane(self, x):
        return -1/self.w[1]*(self.w[0]*x+self.b-.5)

    def accuracy(self, data):
        false = [len(data)-sum(data['Y']), sum(data['Y'])]
        for i in range(len(data)):
            if data['Y'][i] and data['X1'][i] - self.hyperplane(data['X0'][i]) > 0:
                false[1] -= 1
            elif not data['Y'][i] and data['X1'][i] - self.hyperplane(data['X0'][i]) < 0:
                false[0] -=1
        false[1] /= sum(data['Y'])
        false[0] /= (len(data) - sum(data['Y']))
        return false

def main():
    trainingDatasets = ['trainA', 'trainB', 'trainC']
    testDatasets     = ['testA', 'testB', 'testC']
    DATAPATH = '../../data/'
    for i in range(len(trainingDatasets)):
        print('#'*30 +'\t'+ trainingDatasets[i]+'\t'+'#'*30)
        # Loading datasets
        trainingData = loadData(DATAPATH+trainingDatasets[i])
        testData     = loadData(DATAPATH+testDatasets[i])
        # Learning the model
        LIR = LinReg(trainingData)
        LIR.fit()
        acc = LIR.accuracy(trainingData)
        print('Accuracy in the training set:\nCluster1: {:1.4f}\tCluster2: {:1.4f}'.format(acc[0], acc[1]))
        plotData(trainingData, sep=LIR.hyperplane, name='{}_{}'.format(LIR.name, trainingDatasets[i]))
        acc = LIR.accuracy(testData)
        print('Accuracy in the test set:\nCluster1: {:1.4f}\tCluster2: {:1.4f}'.format(acc[0], acc[1]))
        plotData(testData, sep=LIR.hyperplane, name='{}_{}'.format(LIR.name, testDatasets[i]))


if __name__=='__main__':
    main()
