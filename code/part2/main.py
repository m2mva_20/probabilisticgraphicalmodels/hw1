#!/bin/python3

import os
import warnings

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import pandas as pd
import pyreadr
from tqdm import tqdm

warnings.simplefilter('error')

def N(x, mu, sigma):
    if np.linalg.det(sigma) == 0:
        print("Error : Sigma singular")
        return
    n = sigma.shape[0]
    return 1/((2*np.pi)**(n/2)*np.sqrt(abs(np.linalg.det(sigma))))  * np.exp(-1/2*(x.T-mu.T).dot(np.linalg.inv(sigma)).dot(x-mu))[0,0]

def logN(x, mu, sigma):
    if np.linalg.det(sigma) == 0:
        print("Error : Sigma singular")
        return
    n = sigma.shape[0]
    return np.log(1/((2*np.pi)**(n/2)*np.sqrt(abs(np.linalg.det(sigma))))) + (-1/2*(x.T-mu.T).dot(np.linalg.inv(sigma)).dot(x-mu))[0,0]

def E_Step(X,alpha, mu, sigma, n):
    '''Compute the log likelihood's parameters'''
    m = len(alpha)
    tau = np.zeros((n,m))
    for i in range(n):
        den = sum([alpha[k]*N(X[i], mu[k], sigma[k]) for k in range(m)])
        for j in range(m):
            tau[i,j] = alpha[j] * N(X[i], mu[j], sigma[j])/den
    return tau

def M_Step(X, tau, p):
    '''Compute the model's parameters that maximise the log likelihood'''
    n = len(X)
    m = tau.shape[1]
    alpha, mu, sigma = np.zeros(m), np.zeros((m,p, 1)), np.zeros((m,p,p))
    for j in range(m):
        Nj = sum(tau[:,j])
        alpha[j] = 1/n * Nj
        mu[j] = 1/Nj * sum([tau[i,j]*X[i,:] for i in range(n)])
        sigma[j] = 1/Nj * sum([tau[i,j]*(X[i] - mu[j])@(X[i].T - mu[j].T) for i in range(n)])
        if np.linalg.det(sigma[j]) <= 1e-30:
            sigma[j] = np.ones((p,p))*3 + 1e2*np.eye(p)
            mu[j]    = rd.randint(1, 10, size=(p,1))
    return alpha, mu, sigma

def EM_Compute(X, m, n, p, T=100):
    n = len(X)
    mu = np.ones((m,p,1))*np.mean(X)
    sigma = np.zeros((m,p,p)) + np.array([np.eye(p) for i in range(m)]) # Symmetric matrix
    alpha = rd.randint(1,50,m)
    alpha = alpha/sum(alpha)
    tau = E_Step(X,alpha, mu, 1e2*sigma, n)
    LL = np.zeros(T)
    ## Computation with #iteration stopping criteria
    for t in tqdm(range(T)):
        alpha, mu, sigma = M_Step(X, tau,p)
        tau = E_Step(X, alpha, mu, sigma, n)

        for i in range(n):
            for j in range(m):
                LL[t] += tau[i,j] * logN(X[i], mu[j], sigma[j]) + np.log(alpha[j])
    return alpha, mu, sigma, tau, LL

def plotData(data, Y, **kwargs):
    plt.figure()
    plt.scatter(data[:,0,0], data[:,1,0], c=Y, alpha=.6, cmap = 'seismic')
    plt.grid()
    plt.xlabel(r'$X_0$')
    plt.ylabel(r'$X_1$')
    name = '' if 'name' not in kwargs.keys() else kwargs.get('name')
    if 'mu' in kwargs.keys() and 'sigma' in kwargs.keys():
        pi    = kwargs.get('pi')
        sigma = kwargs.get('sigma')
        mu    = kwargs.get('mu')
        x = np.linspace(min(data[:, 0, 0])-1, max(data[:, 0, 0])+1, 100)
        y = np.linspace(min(data[:, 1, 0])-1, max(data[:, 1, 0])+1, 100)
        X,Y = np.meshgrid(x,y)
        for j in range(3):
            z1 = np.zeros((100, 100))
            for i,u in enumerate(x):
                for k,v in enumerate(y):
                    z1[k,i] = N(np.array([[u],[v]]),mu[j], sigma[j])
            levels = [.05]
            CS=plt.contour(X,Y,z1,len(levels),linewidths=0.5, colors = ['black'], levels=levels)
            plt.clabel(CS, inline=True, fontsize=10)
            plt.plot(mu[j][0,0], mu[j][1,0], '+', markersize=10)
    plt.show()


def main():
    # Load data
    data = pyreadr.read_r('../../data/decathlon.RData')['X']
    d = data.to_numpy()
    n,p  = data.shape
    m    = 3
    d = d.reshape(n, p, 1)
    # Compute the EM algo
    alpha, mu, sigma, tau, LL = EM_Compute(d, m, n, p)
    # Projection of the results on a 2D space
    a = sigma[:,0:2,0:2]
    m = mu[:,0:2,:]
    X = d[:,0:2,:]
    # MAP for clustering data
    Y = []
    for i in range(n):
        Y += [np.argmax(tau[i,:])]
    # Plot likelihood and 2D projection of the clustering
    plotData(X, Y, mu = m, sigma = a)
    plt.figure()
    plt.plot(range(len(LL)), LL)
    plt.grid()
    plt.xlabel('#Iterations')
    plt.ylabel('Log likelihood')
    plt.show()


if __name__=="__main__":
    main()
